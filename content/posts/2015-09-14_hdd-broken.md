Title: HDD がなんか調子おかしい
Date: 2015-09-14
Category: Server
Tags: linux, server, hdd

```log
[ 3954.492094] ata3.00: exception Emask 0x10 SAct 0x300 SErr 0x400000 action 0x6 frozen
[ 3954.492135] ata3.00: irq_stat 0x08000000, interface fatal error
[ 3954.492159] ata3: SError: { Handshk }
[ 3954.492181] ata3.00: failed command: WRITE FPDMA QUEUED
[ 3954.492210] ata3.00: cmd 61/40:40:62:d6:c0/05:00:0d:00:00/40 tag 8 ncq 688128 out
[ 3954.492268] ata3.00: status: { DRDY }
[ 3954.492288] ata3.00: failed command: WRITE FPDMA QUEUED
[ 3954.492315] ata3.00: cmd 61/40:48:a2:db:c0/05:00:0d:00:00/40 tag 9 ncq 688128 out
[ 3954.492371] ata3.00: status: { DRDY }
[ 3954.492395] ata3: hard resetting link
[ 3959.852050] ata3: link is slow to respond, please be patient (ready=0)
[ 3964.500050] ata3: COMRESET failed (errno=-16)
[ 3964.500083] ata3: hard resetting link
[ 3969.860050] ata3: link is slow to respond, please be patient (ready=0)
[ 3974.508049] ata3: COMRESET failed (errno=-16)
[ 3974.508082] ata3: hard resetting link
[ 3974.828051] ata3: SATA link up 1.5 Gbps (SStatus 113 SControl 310)
[ 3974.840859] ata3.00: configured for UDMA/33
[ 3974.840887] ata3: EH complete
```

~~どうやら、SATAケーブルの接触不良とかそんな感じらしい。。。~~

~~[弁財天: sataケーブルの接続エラー](http://benzaiten.dyndns.org/roller/ugya/entry/sataケーブルの接続エラー)~~

A330IONのBiosバージョンが原因でした。

[http://ubuntuforums.org/archive/index.php/t-1608032.html](http://ubuntuforums.org/archive/index.php/t-1608032.html)

[Bois V1.20](/A330ION(1.20)ROM.zip) にして正常になりました。

reboot 毎に 3.0Gbps <-> 1.5Gbps と切り替わるけど、もうしょうがないか。。。  
SATAケーブル変えても改善しなかったし。。。
