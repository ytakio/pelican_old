Title: Markdown cheat sheet
Date: 2015-10-04
Category: Markdown
Tags: markdown

# BLOCK ELEMENTS

## PARAGRAPHS AND LINE BREAKS (段落)

段落は一つ以上の空行で分けられる。

**e.g.**

```markdown
aaa. bbb.

aaa.
bbb.
```

aaa. bbb.

aaa.
bbb.

どうしても改行を入れたい場合は、行末に2個以上の空スペースを入れて改行をすればOK。

**e.g.**

```markdown
aaa.  
bbb.
```

aaa.  
bbb.

## HEADERS (見出し)

見出しは二種類

**Setext-style headers**

```markdown
This is an H1
=============

This is an H2
-------------
```

This is an H1
=============

This is an H2
-------------

**Atx-style headers**

```markdown
# This is an H1

## This is an H2

###### This is an H6

# This is an H1 #

## This is an H2 ##

### This is an H3 ######
```

# This is an H1

## This is an H2

###### This is an H6

# This is an H1 #

## This is an H2 ##

### This is an H3 ######

後半のはオプション、閉じる # の数は合って無くてもOK。

## BLOCKQUOTES (引用)

引用はメールで使うような、一般的なもの。

```markdown
> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.
> 
> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
> id sem consectetuer libero luctus adipiscing.
> 
> This is the first level of quoting.
>
> > This is nested blockquote.
>
> Back to the first level.
```

> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.
> 
> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
> id sem consectetuer libero luctus adipiscing.
> 
> This is the first level of quoting.
>
> > This is nested blockquote.
>
> Back to the first level.

引用内にMarkdownが含まれててもOK

```markdown
> ## This is a header.
> 
> 1.   This is the first list item.
> 2.   This is the second list item.
> 
> Here's some example code:
> 
>     return shell_exec("echo $input | $markdown_script");
```

> ## This is a header.
> 
> 1.   This is the first list item.
> 2.   This is the second list item.
> 
> Here's some example code:
> 
>     return shell_exec("echo $input | $markdown_script");

## LISTS (リスト)

以下のように決まった文字を先頭に付けてインデント揃えてスペースを入れれば箇条書きに

**箇条書き**

```markdown
*   Red
*   Green
*   Blue

+   Red
+   Green
+   Blue

-   Red
-   Green
-   Blue
```

*   Red
*   Green
*   Blue

+   Red
+   Green
+   Blue

-   Red
-   Green
-   Blue

**番号**

```markdown
1.  Bird
1.  McHale
1.  Parish

break

3. Bird
1. McHale
8. Parish
```

1.  Bird
1.  McHale
1.  Parish

break

3. Bird
1. McHale
8. Parish


### リスト内インデント

リスト内で通常の段落の記述が適応されるが、インデントに関してはスペース4毎に追加される。

```markdown
1.  This is a list item with two paragraphs. Lorem ipsum dolor
    sit amet, consectetuer adipiscing elit. Aliquam hendrerit
    mi posuere lectus.

    Vestibulum enim wisi, viverra nec, fringilla in, laoreet
    vitae, risus. Donec sit amet nisl. Aliquam semper ipsum
    sit amet velit.

2.  Suspendisse id sem consectetuer libero luctus adipiscing.

*   This is a list item with two paragraphs.

    This is the second paragraph in the list item. You're
only required to indent the first line. Lorem ipsum dolor
sit amet, consectetuer adipiscing elit.

*   Another item in the same list.

*   A list item with a blockquote:

    > This is a blockquote
    > inside a list item.

*   A list item with a code block:

    ````c
    void main(void);
    ````
```

1.  This is a list item with two paragraphs. Lorem ipsum dolor
    sit amet, consectetuer adipiscing elit. Aliquam hendrerit
    mi posuere lectus.

    Vestibulum enim wisi, viverra nec, fringilla in, laoreet
    vitae, risus. Donec sit amet nisl. Aliquam semper ipsum
    sit amet velit.

2.  Suspendisse id sem consectetuer libero luctus adipiscing.

*   This is a list item with two paragraphs.

    This is the second paragraph in the list item. You're
only required to indent the first line. Lorem ipsum dolor
sit amet, consectetuer adipiscing elit.

*   Another item in the same list.

*   A list item with a blockquote:

    > This is a blockquote
    > inside a list item.

*   A list item with a code block:

    ````c
    void main(void);
    ````

## HORIZONTAL RULES (区切り線)

区切り線は決まった文字を繰り返すと出せる。見栄えのために間にスペース入れてもOK。

```markdown
a

* * *
b

***
c

*****
d

- - -
e

---------------------------------------
f
```

a

* * *
b

***
c

*****
d

- - -
e

---------------------------------------
f

# SPAN ELEMENTS (まぁ文字とか単語への修飾、リンクとか)

## LINKS

```markdown
This is [an example](http://example.com/ "Title") inline link.

[This link](http://example.net/) has no title attribute.

This is [an example][id] reference-style link.

[id]: http://example.com/
    "Optional Title Here"

[Google Japan][]

[Google Japan]: http://google.co.jp/
```

This is [an example](http://example.com/ "Title") inline link.

[This link](http://example.net/) has no title attribute.


This is [an example][id] reference-style link.

[id]: http://example.com/
    "Optional Title Here"

[Google Japan][]

[Google Japan]: http://google.co.jp/

## EMPHASIS (強調)

```markdown
*single asterisks*

_single underscores_

**double asterisks**

__double underscores__

un*frigging*believable

\*this text is surrounded by literal asterisks\*
```

*single asterisks*

_single underscores_

**double asterisks**

__double underscores__

un*frigging*believable

\*this text is surrounded by literal asterisks\*

## CODE

`` ` `` こいつを使って引用するが、コード内で使いたい場合はopen/close時の数を揃える。

```markdown
Use the `printf()` function.

``There is a literal backtick (`) here.``
```

Use the `printf()` function.

``There is a literal backtick (`) here.``


## IMAGES

[リンク](/MarkdownOriginalSheet#links)と一緒。`!`を先頭に付けだけ。


```markdown
![woof!](/img/logo.png)
```

![woof!](/img/logo.png)

# MISCELLANEOUS (その他)

## AUTO LINKS

```markdown
<https://google.co.jp>

<hogehoge@gmail.com>
```

<https://google.co.jp>

<hogehoge@gmail.com>

